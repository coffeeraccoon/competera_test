from aiohttp import web

from views import DelayView


app = web.Application()
app.router.add_route("*", "/delay", DelayView)
web.run_app(app)
