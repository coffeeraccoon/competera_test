from aiohttp import web
from functions import get_delay, get_new_completed_rq_counts, time_epsilon
from datetime import datetime
import settings
import models


class DelayView(web.View):
    async def get(self):
        if "url" not in self.request.GET:
            return web.HTTPBadRequest()
        if "prev_response_time" not in self.request.GET:
            prev_response_time = None
        else:
            prev_response_time = self.request.GET["prev_response_time"]
            prev_response_time = datetime.strptime(prev_response_time, "%Y-%m-%d-%H-%M-%S-%f")
        domain = self.request.GET["url"]
        session = models.Session()
        url = session.query(models.Url).filter(models.Url.domain == domain).first()
        if url is None:
            url = models.Url(domain)
            session.add(url)
            session.commit()
            return web.Response(text="0")
        delay = get_delay(url, prev_response_time)
        return web.Response(text=str(delay))
