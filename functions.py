from datetime import datetime
from sqlalchemy import desc
import settings
import models
# In this model, we assume that 1 unit of time is 1 second
time_epsilon = 0.1  # epsilon


def get_delay(url, prev_response_time=None):
    session = models.Session()
    url = session.query(models.Url).filter(models.Url.id == url.id).first()
    if prev_response_time is not None:
        periods = session.query(models.Period).filter(models.Period.url_id == url.id)
        if periods.count() == 0:
            period = models.Period(url.id, settings.RQ_TIME, 1, 1)
            session.add(period)
            session.commit()
            url.prev_response_time = prev_response_time
            session.commit()
            session.close()
            return 0
        if periods.count() == 1:
            last_period = periods.first()
            if last_period.time == settings.RQ_TIME and last_period.rq_count == 1:
                cur_frequency = prev_response_time - url.prev_response_time
                cur_frequency = cur_frequency.total_seconds()
                last_period.time = cur_frequency
                last_period.rq_count += 1
        else:
            last_period = periods.order_by(desc(models.Period.order)).first()
            last_period_frequency = last_period.rq_count / last_period.time
            cur_frequency = prev_response_time - url.prev_response_time
            cur_frequency = cur_frequency.total_seconds()
            if abs(last_period_frequency - cur_frequency) <= time_epsilon:
                last_period.time = last_period.time + cur_frequency
                last_period.rq_count += 1
                session.commit()
            else:
                period = models.Period(url.id, cur_frequency, 1, last_period.order + 1)
                session.add(period)
                session.commit()
    periods = session.query(models.Period).filter(models.Period.url_id == url.id).order_by(models.Period.order)
    time_diff = datetime.now() - url.prev_response_time
    time_diff = time_diff.total_seconds()
    i = 0
    old_periods_count = periods.count()
    old_last_period = tuple()
    deleted_periods = list()
    while time_diff > 0 and i < old_periods_count:
        time_diff -= periods[i].time
        old_last_period = (periods[i].time, periods[i].rq_count, periods[i].order)
        deleted_periods.append(periods[i].order)
        i += 1
    for j in deleted_periods:
        session.query(models.Period).filter(models.Period.order == j).delete()
    session.commit()
    if i != old_periods_count:
        if time_diff < 0:
            i -= 1
            period_frequency = old_last_period[1] / old_last_period[0]
            period_time = -time_diff
            period_count = round(period_time * period_frequency)
            period = models.Period(url.id, period_time, period_count, old_last_period[2])
            session.add(period)
            session.commit()
    if prev_response_time is not None:
        url.prev_response_time = prev_response_time
        session.commit()
    time_diff = datetime.now() - url.prev_response_time
    time_diff = time_diff.total_seconds()
    completed_rq_sum = 0
    periods = session.query(models.Period).filter(models.Period.url_id == url.id)
    for period in periods:
        completed_rq_sum += period.rq_count
    rq_delay = (settings.RQ_COUNT - completed_rq_sum) / time_diff
    session.close()
    return rq_delay
