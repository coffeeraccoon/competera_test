DB_DEBUG = True
DB_DRIVER = "mysql+pymysql"
DB_HOST = "127.0.0.1"
DB_PORT = "3306"
DB_USER = "competera_user"
DB_PASS = "competera_pass"
DB_NAME = "competera_test"

RQ_TIME = 10
RQ_COUNT = 150