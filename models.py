import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import settings


BaseModel = declarative_base()
metadata = BaseModel.metadata
engine = sa.create_engine("{:s}://{:s}:{:s}@{:s}:{:s}/{:s}".format(settings.DB_DRIVER,
                                                                   settings.DB_USER,
                                                                   settings.DB_PASS,
                                                                   settings.DB_HOST,
                                                                   settings.DB_PORT,
                                                                   settings.DB_NAME),
                          echo=True if settings.DB_DEBUG else False)
Session = sessionmaker(bind=engine)


class Period(BaseModel):
    __tablename__ = "periods"

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    time = sa.Column(sa.Float)
    rq_count = sa.Column(sa.Integer)
    order = sa.Column(sa.Integer)
    url_id = sa.Column(sa.Integer, sa.ForeignKey("urls.id"))

    def __init__(self, url_id, time, rq_count, order):
        self.url_id = url_id
        self.time = time
        self.rq_count = rq_count
        self.order = order

    def __repr__(self):
        return "<Period ({:d})>".format(self.id)


class Url(BaseModel):
    __tablename__ = "urls"

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    domain = sa.Column(sa.String(255))
    prev_response_time = sa.Column(sa.DateTime, nullable=True)

    def __init__(self, domain):
        self.domain = domain

    def __repr__(self):
        return "<URL ({:d})>".format(self.id)

if __name__ == "__main__":
    BaseModel.metadata.create_all(bind=engine)
    print("Successful migration.")
